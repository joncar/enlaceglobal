<?php

class ajax_grocery_CRUD extends grocery_CRUD {

	protected $unset_ajax_extension			= false;
	protected $state_code 			= null;
	private $slash_replacement	= "_agsl_";
	protected $relation_dependency		= array();

	function __construct()
	{
		parent::__construct();

		$this->states[101]='ajax_extension';

	}

	public function inline_js($inline_js = '')
	{
		$this->_inline_js($inline_js);
	}


	public function set_relation_dependency($target_field, $source_field, $relation_field_on_source_table)
	{
		$this->relation_dependency[$target_field] = array($target_field, $source_field,$relation_field_on_source_table);
		return $this;
	}

	private function render_relation_dependencies()
	{

		foreach($this->relation_dependency as $dependency)
		{
			$this->render_relation_dependency($dependency[0],$dependency[1],$dependency[2]);
		}

	}

	private function render_relation_dependency($target_field, $source_field, $relation_field_on_source_table){

		$sourceElement = "'#field-$source_field'";
		$targetElement = "'#field-$target_field'";

		$js_text = "
			$(document).ready(function() {
				$($sourceElement).change(function() {
					var selectedValue = $($sourceElement).val();
					//alert('selectedValue'+selectedValue);
					//alert('post:'+'ajax_extension/$target_field/$relation_field_on_source_table/'+encodeURI(selectedValue.replace(/\//g,'$this->slash_replacement')));
					$.post('ajax_extension/$target_field/$relation_field_on_source_table/'+encodeURI(selectedValue.replace(/\//g,'$this->slash_replacement')), {}, function(data) {
					//alert('data'+data);
					var \$el = $($targetElement);
						  var newOptions = data;
						  \$el.empty(); // remove old options
						  \$el.append(\$('<option></option>').attr('value', '').text(''));
						  \$.each(newOptions, function(key, value) {
						    \$el.append(\$('<option></option>')
						       .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
						    });
						  //\$el.attr('selectedIndex', '-1');
						  \$el.chosen().trigger('liszt:updated');

    	  			},'json');
    	  			$($targetElement).change();
				});
			});
			";

		$this->inline_js($js_text);

	}

	public function render()
	{

		$this->pre_render();

		$this->state_code = $this->getStateCode();

		if( $this->state_code != 0 )
		{
			$this->state_info = $this->getStateInfo();
		}
		else
		{
			throw new Exception('The state is unknown , I don\'t know what I will do with your data!', 4);
			die();
		}

		switch ($this->state_code) {
			case 2://add
					$this->render_relation_dependencies();
					$output = parent::render();
			break;
			case 3://edit
					$this->render_relation_dependencies();
					$output = parent::render();
			break;
			case 6://update
				$this->render_relation_dependencies();
				$output = parent::render();
			break;

			case 101://ajax_extension

				$state_info = $this->getStateInfo();

				$ajax_extension_result = $this->ajax_extension($state_info);

				$ajax_extension_result[""] = "";

				echo json_encode($ajax_extension_result);
			die();

			break;
			default:

				$output = parent::render();

			break;

		}

		if(empty($output)){
			$output = $this->get_layout();
		}else{
		}

		return $output;
	}



public function getStateInfo()
	{
		$state_code = $this->getStateCode();

		$segment_object = $this->get_state_info_from_url();

		$first_parameter = $segment_object->first_parameter;

		$second_parameter = $segment_object->second_parameter;

		$third_parameter = $segment_object->third_parameter;


		$state_info = (object)array();

		switch ($state_code) {
			case 101: //ajax_extension
				$state_info->target_field_name = $first_parameter;
				$state_info->relation_field_on_source_table = $second_parameter;
				$state_info->filter_value = $third_parameter;

			break;

			default:
				$state_info = parent::getStateInfo();

		}

		return $state_info;
	}



	protected function ajax_extension($state_info)
	{

		if(!isset($this->relation[$state_info->target_field_name]))
			return false;

		list($field_name, $related_table, $related_field_title, $where_clause, $order_by)  = $this->relation[$state_info->target_field_name];


		$target_field_name = $state_info->target_field_name;

		$relation_field_on_source_table = $state_info->relation_field_on_source_table;

		$filter_value = $state_info->filter_value;

		if(is_int($filter_value)){

			$final_filter_value = $filter_value;

		}else {

				$decoded_filter_value = urldecode($filter_value);

				$replaced_filter_value = str_replace($this->slash_replacement,'/',$decoded_filter_value);

				if(strpos($replaced_filter_value,'/') !== false) {
					$final_filter_value = $this->_convert_date_to_sql_date($replaced_filter_value);

				}else{
					$final_filter_value = $replaced_filter_value;
				}
		}

		$target_field_relation = $this->relation[$target_field_name];

		$result = $this->get_dependency_relation_array($target_field_relation, $relation_field_on_source_table, $final_filter_value);

		return $result;
	}

	protected function get_dependency_relation_array($relation_info, $relation_key_field, $relation_key_value, $limit = null)
	{
		list($field_name , $related_table , $related_field_title, $where_clause, $order_by)  = $relation_info;

		$where_clause = array($relation_key_field => $relation_key_value);
		if(empty($relation_key_value)){
			$relation_array = array();
		}else{
			$relation_array = $this->basic_model->get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit);
		}
		return $relation_array;
	}



	public function unset_ajax_extension()
	{
		$this->unset_ajax_extension = true;

		return $this;
	}


    //Overriden with the purpose of adding a third parameter, currently not calling parent. It should be changed in future if changes are made to parent.
	protected function get_state_info_from_url()
	{
		$ci = &get_instance();

		$segment_position = count($ci->uri->segments) + 1;
		$operation = 'list';

		$segements = $ci->uri->segments;
		foreach($segements as $num => $value)
		{
			if($value != 'unknown' && in_array($value, $this->states))
			{
				$segment_position = (int)$num;
				$operation = $value; //I don't have a "break" here because I want to ensure that is the LAST segment with name that is in the array.
			}
		}

		$function_name = $this->get_method_name();

		if($function_name == 'index' && !in_array('index',$ci->uri->segments))
			$segment_position++;

		$first_parameter = isset($segements[$segment_position+1]) ? $segements[$segment_position+1] : null;
		$second_parameter = isset($segements[$segment_position+2]) ? $segements[$segment_position+2] : null;
		$third_parameter = isset($segements[$segment_position+3]) ? $segements[$segment_position+3] : null;

		return (object)array('segment_position' => $segment_position, 'operation' => $operation, 'first_parameter' => $first_parameter, 'second_parameter' => $second_parameter, 'third_parameter' => $third_parameter);
	}
        
        protected function get_field_input($field_info, $value = null)
	{
			$real_type = $field_info->crud_type;

			$types_array = array(
					'integer',
					'text',
					'true_false',
					'string',
					'date',
					'datetime',
					'enum',
					'set',
					'relation',
					'relation_readonly',
					'relation_n_n',
					'upload_file',
					'upload_file_readonly',
					'hidden',
					'password',
					'readonly',
					'dropdown',
					'multiselect',
                                        'tags',
                                        'map',
                                        'editor'
			);

			if (in_array($real_type,$types_array)) {
				/* A quick way to go to an internal method of type $this->get_{type}_input .
				 * For example if the real type is integer then we will use the method
				 * $this->get_integer_input
				 *  */
				$field_info->input = $this->{"get_".$real_type."_input"}($field_info,$value);
			}
			else
			{
				$field_info->input = $this->get_string_input($field_info,$value);
			}

		return $field_info;
	}
        
        protected function _initialize_variables()
	{
		$ci = &get_instance();
		$ci->load->config('grocery_crud');

		$this->config = (object)array();

		/** Initialize all the config variables into this object */
		$this->config->default_language 	= $ci->config->item('grocery_crud_default_language');
		$this->config->date_format 			= $ci->config->item('grocery_crud_date_format');
		$this->config->default_per_page		= $ci->config->item('grocery_crud_default_per_page');
		$this->config->file_upload_allow_file_types	= $ci->config->item('grocery_crud_file_upload_allow_file_types');
		$this->config->file_upload_max_file_size	= $ci->config->item('grocery_crud_file_upload_max_file_size');
		$this->config->default_text_editor	= $ci->config->item('grocery_crud_default_text_editor');
		$this->config->text_editor_type		= $ci->config->item('grocery_crud_text_editor_type');
		$this->config->character_limiter	= $ci->config->item('grocery_crud_character_limiter');
		$this->config->dialog_forms			= $ci->config->item('grocery_crud_dialog_forms');
		$this->config->paging_options		= $ci->config->item('grocery_crud_paging_options');
                $this->config->map_lib = $ci->config->item('map_lib');
                $this->config->map_lat = $ci->config->item('map_lat');
                $this->config->map_lon = $ci->config->item('map_lon');
		/** Initialize default paths */
		$this->default_javascript_path				= $this->default_assets_path.'/js';
		$this->default_css_path						= $this->default_assets_path.'/css';
		$this->default_texteditor_path 				= $this->default_assets_path.'/texteditor';
		$this->default_theme_path					= $this->default_assets_path.'/themes';

		$this->character_limiter = $this->config->character_limiter;

		if($this->character_limiter === 0 || $this->character_limiter === '0')
		{
			$this->character_limiter = 1000000; //a big number
		}
		elseif($this->character_limiter === null || $this->character_limiter === false)
		{
			$this->character_limiter = 30; //is better to have the number 30 rather than the 0 value
		}
	}
        
        public function set_js_lib($js_file,$url=false)
	{
		$this->js_lib_files[sha1($js_file)] = !$url?base_url().$js_file:$js_file;
		$this->js_files[sha1($js_file)] = !$url?base_url().$js_file:$js_file;
	}
        
        protected function get_tags_input($field_info,$value)
	{		
                    //$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/tags.js');
                    //$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/tags.js');
                    $this->set_css($this->default_css_path.'/jquery_plugins/tags.css');
                    
                    $str = '';
                    if(!empty($field_info->extras)){
                    foreach($field_info->extras as $e)
                    $str.= '"'.$e.'",';
                    }
                    $input =
                            '<script src="'.base_url('js/tags.js').'"></script>'.                             
                            form_input($field_info->name,$value,'id="field-'.$field_info->name.'" class="tags"').
                            '<script>$("#field-'.$field_info->name.'").tagbox({url:['.$str.']});</script>';
                    return $input;
	}
        
        protected function get_map_input($field_info,$value)
	{		
            $this->set_js_lib($this->config->map_lib,true);
            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/map.js');
            $extras = $field_info->extras;
            $width = !empty($extras['width'])?$extras['width']:'400px';
            $height = !empty($extras['height'])?$extras['height']:'400px';
            $lat = !empty($extras['lat'])?$extras['lat']:$this->config->map_lat;
            $lon = !empty($extras['lon'])?$extras['lon']:$this->config->map_lon;
            if(!empty($value)){
                $c = explode(',',$value);
                $lat = str_replace('(','',$c[0]);
                $lon = str_replace(')','',$c[1]);
            }
            return                    
                   '<input type="hidden" value="'.$value.'" name="'.$field_info->name.'" id="field-'.$field_info->name.'">'
                   .'<div id="map_'.$field_info->name.'" style="width:'.$width.'; height:'.$height.'"></div>'
                   .'<script>
                    var '.$field_info->name.' = new mapa(\'map_'.$field_info->name.'\',\''.$lat.'\',\''.$lon.'\');'.
                    $field_info->name.'.initialize();
                    google.maps.event.addListener('.$field_info->name.'.marker,\'dragend\',function(){$("#field-'.$field_info->name.'").val('.$field_info->name.'.marker.getPosition())});
                    </script>';
	}
        
        protected function get_editor_input($field_info,$value)
	{
            $extras = $field_info->extras;
           
		if($extras['type'] == 'text_editor')
		{
			$editor = empty($extras['editor'])?$this->config->default_text_editor:$extras['editor'];
			switch ($editor) {
				case 'ckeditor':
					$this->set_js_lib($this->default_texteditor_path.'/ckeditor/ckeditor.js');
					$this->set_js_lib($this->default_texteditor_path.'/ckeditor/adapters/jquery.js');
					$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.ckeditor.config.js');
				break;

				case 'tinymce':
					$this->set_js_lib($this->default_texteditor_path.'/tiny_mce/jquery.tinymce.js');
					$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.tine_mce.config.js');
				break;

				case 'markitup':
					$this->set_css($this->default_texteditor_path.'/markitup/skins/markitup/style.css');
					$this->set_css($this->default_texteditor_path.'/markitup/sets/default/style.css');

					$this->set_js_lib($this->default_texteditor_path.'/markitup/jquery.markitup.js');
					$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.markitup.config.js');
				break;
			}

			$class_name = $this->config->text_editor_type == 'minimal' ? 'mini-texteditor' : 'texteditor';

			$input = "<textarea id='field-{$field_info->name}' name='{$field_info->name}' class='$class_name' >$value</textarea>";
		}
		else
		{
			$input = "<textarea id='field-{$field_info->name}' name='{$field_info->name}'>$value</textarea>";
		}
		return $input;
	}

}
