<?php
$fecha = traer_lunes($fecha);
$pdf = $this->fpdf;
$pdf->AddPage('L');
$pdf->SetFillColor(142,180,227);
/*Cabecera*/
$pdf->SetFont('Arial','B',14);
$pdf->cell(0,14,'RESUMEN SEMANAL',0,2,'C');
$pdf->SetFont('Arial','',10);
$pdf->Image(base_url('img/logo.png'), 10 ,10, 50 , 20,'PNG');
$pdf->Ln();
$pdf->cell(0,5,'Semana: '.date("d/m/Y",strtotime($fecha)),0,2,'L');
$pdf->cell(0,5,'Promotor: '.$promotor->row()->nombre,0,2,'L');
$pdf->Ln();
$pdf->cell(41,5,'Zona',1,0,'L',true);
$pdf->cell(21,5,'Deb. Entregar',1,0,'C',true);
$pdf->cell(21,5,'Entrego',1,0,'C',true);
$pdf->cell(21,5,'Diferencia',1,0,'C',true);
$pdf->cell(21,5,'Recuperado',1,0,'C',true);
$pdf->cell(21,5,'Rec TV',1,0,'C',true);
$pdf->cell(21,5,'Deuda',1,0,'C',true);
$pdf->cell(21,5,'Credito',1,0,'C',true);
$pdf->cell(41,5,'Com. Apertura',1,0,'C',true);
$pdf->cell(21,5,'Comision',1,0,'C',true);
$pdf->cell(21,5,'Abono',1,0,'C',true);
$t1 = $t2 = $t3 = $t4 = $t5 = $t6 = $t7 = $t8 = $t9 = $t10 = 0;
foreach($res->result() as $r)
{
    $pdf->Ln();
    $pdf->cell(41,5,$r->suc_nombre,1,0,'L');
    
    $this->db->where('solicitudes.sucursal',$r->sucid);
    $solicitudes = $this->db->get('solicitudes');
    foreach($solicitudes->result() as $s)
        $this->db->or_where("fecha = '".$fecha."' AND solicitud='".$s->id."'");    
    $entregar = 0;
    $pago = 0;
    $solicitud = 0;
    $saldo = 0;
    $recuperado = 0;
    foreach($this->db->get('estado_cuenta')->result() as $x)
    {
        $saldo+= $solicitud!=$x->solicitud?$x->saldo:0;
        $solicitud = $x->solicitud;
        $entregar+= $x->abono;
        $pago+= $x->pago;
        $rec= $x->abono-$x->pago;
        $recuperado += $rec<0?($rec*-1):0;
    }
    $pdf->cell(21,5,number_format($entregar,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($pago,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($entregar-$pago,2,',','.'),1,0,'C');
    $pdf->cell(21,5,0,1,0,'C');
    $pdf->cell(21,5,0,1,0,'C');
    $pdf->cell(21,5,number_format($entregar-$pago,2,',','.'),1,0,'C');    
    $pdf->cell(21,5,number_format($recuperado,2,',','.'),1,0,'C');
    $pdf->cell(41,5,number_format($saldo*0.07,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($saldo*$r->comision,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($saldo*0.15,2,',','.'),1,0,'C');
    $t1 += $entregar;
    $t2 += $pago;
    $t3 += $entregar-$pago;
    $t4 += $recuperado;
    $t5 += 0;
    $t6 += $entregar-$pago;
    $t7 += $saldo;
    $t8 += $saldo*0.07;
    $t9 += $saldo*$r->comision;
    $t10+= $saldo*0.15;
}
$pdf->Ln();
$pdf->cell(41,5,'Totales',1,0,'L',true);
$pdf->cell(21,5,number_format($t1,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t2,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t3,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t4,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t5,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t6,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t7,2,',','.'),1,0,'C',true);
$pdf->cell(41,5,number_format($t8,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t9,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t10,2,',','.'),1,0,'C',true);
$pdf->Output();
?>
