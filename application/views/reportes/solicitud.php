<?php
$this->fpdf->AddPage();
/*Cabecera*/
$this->fpdf->SetFont('Arial','B',14);
$this->fpdf->cell(190,14,'SOLICITUD DE CREDITO',0,2,'C');
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Image(base_url('img/logo.png'), 10 ,10, 50 , 20,'PNG');
$this->fpdf->SetXY(10,20);
$this->fpdf->cell(190,14,'Fecha: '.date("d/m/Y",strtotime($solicitud->fecha)),0,2,'C');
$this->fpdf->SetXY(170,20);
$this->fpdf->cell(20,6,'Monto Solicitado: '.$solicitud->monto_solicitado,0,2,'R');
$this->fpdf->cell(20,6,'Plazo: '.$solicitud->plazo,0,2,'R');
$this->fpdf->cell(20,6,'Monto Aprobado: '.$solicitud->monto_aprobado,0,2,'R');
$this->fpdf->SetXY(10,50);
$this->fpdf->cell(60,0,'Zona: '.$solicitud->sucursalnombre,0,2,'L');
$this->fpdf->SetXY(70,47);
$this->fpdf->cell(30,6,$solicitud->sucursal,1,2,'C',false);
$this->fpdf->SetXY(100,47);
$this->fpdf->cell(30,6,$solicitud->clienteid,1,2,'C');
$this->fpdf->SetXY(130,47);
$this->fpdf->cell(30,6,$solicitud->id,1,2,'C');
$this->fpdf->SetXY(10,55);
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->SetXY(70,55);
$this->fpdf->cell(30,0,'No. Zona',0,2,'C');
$this->fpdf->SetXY(100,55);
$this->fpdf->cell(30,0,'No. Cliente',0,2,'C');
$this->fpdf->SetXY(130,55);
$this->fpdf->cell(30,0,'No. Solicitud',0,2,'C');

/*Fin de cabecera*/
/* Datos del cliente */
$this->fpdf->SetFont('Arial','B',12);
$this->fpdf->SetXY(10,60);
$this->fpdf->SetFillColor(191,191,191);
$this->fpdf->cell(30,6,'Solicitante',0,2,'L');
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(63.3,5,'Nombre(s)',1,0,'L',TRUE);
$this->fpdf->cell(63.3,5,'Apellido Paterno',1,0,'L',TRUE);
$this->fpdf->cell(63.3,5,'Apellido Materno',1,0,'L',TRUE);
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->cell(63.3,6,$solicitud->cliente_nombre,1,0,'L');
$this->fpdf->cell(63.3,6,$solicitud->cliente_apellido_paterno,1,0,'L');
$this->fpdf->cell(63.3,6,$solicitud->cliente_apellido_materno,1,0,'L');
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(80,5,'Calle y Numero',1,0,'C',TRUE);
$this->fpdf->cell(46.6,5,'Colonia',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Ciudad',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Estado',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'C.P',1,0,'C',TRUE);
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Ln();
$this->fpdf->cell(80,5,$solicitud->cliente_direccion,1,0,'L');
$this->fpdf->cell(46.6,5,$this->db->get_where('colonias',array('id'=>$solicitud->cliente_colonia))->row()->nombre,1,0,'L');
$this->fpdf->cell(21.1,5,$this->db->get_where('ciudades',array('id'=>$solicitud->cliente_ciudad))->row()->nombre,1,0,'L');
$this->fpdf->cell(21.1,5,$this->db->get_where('estados',array('id'=>$solicitud->cliente_estado))->row()->nombre,1,0,'L');
$this->fpdf->cell(21.1,5,$solicitud->cliente_zip,1,0,'L');
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(60,5,'Folio de IFE',1,0,'C',TRUE);
$this->fpdf->cell(66.5,5,'Negocio o Actividad',1,0,'C',TRUE);
$this->fpdf->cell(42.2,5,'Telefono Celular',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Tel. Casa',1,0,'C',TRUE);
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Ln();
$this->fpdf->cell(60,5,$solicitud->cliente_ife,1,0,'C');
$this->fpdf->cell(66.5,5,$solicitud->cnegocio_actividad,1,0,'C');
$this->fpdf->cell(42.2,5,$solicitud->ccelular,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->ctelefono,1,0,'C');
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(20,5,'SEXO',1,0,'C',TRUE);
$this->fpdf->cell(20,5,'EDAD',1,0,'C',TRUE);
$this->fpdf->cell(20,5,'Estado Civil',1,0,'C',TRUE);
$this->fpdf->cell(22.16,5,'C. Propia',1,0,'C',TRUE);
$this->fpdf->cell(22.16,5,'C. Rentada',1,0,'C',TRUE);
$this->fpdf->cell(22.16,5,utf8_decode('Años Res'),1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Ing. Sem.',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Ing. Extra',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Total',1,0,'C',TRUE);
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Ln();
$c1 = $solicitud->ccasa_propia == 0?'NO':'SI';
$c2 = $solicitud->ccasa_rentada == 0?'NO':'SI';
$this->fpdf->cell(20,5,$solicitud->csexo,1,0,'C');
$this->fpdf->cell(20,5,$solicitud->cedad,1,0,'C');
$this->fpdf->cell(20,5,$solicitud->cestado_civil,1,0,'C');
$this->fpdf->cell(22.16,5,$c1,1,0,'C');
$this->fpdf->cell(22.16,5,$c2,1,0,'C');
$this->fpdf->cell(22.16,5,$solicitud->canos,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->cingreso,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->cextra,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->ctotal,1,0,'C');
/* Fin de solicitante */
/* AVAL */
$this->fpdf->SetFont('Arial','B',12);
$this->fpdf->SetXY(10,110);
$this->fpdf->SetFillColor(191,191,191);
$this->fpdf->cell(30,6,'AVAL',0,2,'L');
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(63.3,5,'Nombre(s)',1,0,'L',TRUE);
$this->fpdf->cell(63.3,5,'Apellido Paterno',1,0,'L',TRUE);
$this->fpdf->cell(63.3,5,'Apellido Materno',1,0,'L',TRUE);
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->cell(63.3,6,$solicitud->nombre,1,0,'L');
$this->fpdf->cell(63.3,6,$solicitud->apellido_paterno,1,0,'L');
$this->fpdf->cell(63.3,6,$solicitud->apellido_materno,1,0,'L');
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(80,5,'Calle y Numero',1,0,'C',TRUE);
$this->fpdf->cell(46.6,5,'Colonia',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Ciudad',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Estado',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'C.P',1,0,'C',TRUE);
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Ln();
$this->fpdf->cell(80,5,$solicitud->direccion,1,0,'L');
$this->fpdf->cell(46.6,5,$this->db->get_where('colonias',array('id'=>$solicitud->colonia))->row()->nombre,1,0,'L');
$this->fpdf->cell(21.1,5,$this->db->get_where('ciudades',array('id'=>$solicitud->ciudad))->row()->nombre,1,0,'L');
$this->fpdf->cell(21.1,5,$this->db->get_where('estados',array('id'=>$solicitud->estado))->row()->nombre,1,0,'L');
$this->fpdf->cell(21.1,5,$solicitud->zip,1,0,'L');
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(60,5,'Folio de IFE',1,0,'C',TRUE);
$this->fpdf->cell(66.5,5,'Negocio o Actividad',1,0,'C',TRUE);
$this->fpdf->cell(42.2,5,'Telefono Celular',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Tel. Casa',1,0,'C',TRUE);
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Ln();
$this->fpdf->cell(60,5,$solicitud->folio_ife,1,0,'C');
$this->fpdf->cell(66.5,5,$solicitud->negocio_actividad,1,0,'C');
$this->fpdf->cell(42.2,5,$solicitud->celular,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->telefono,1,0,'C');
$this->fpdf->Ln();
$this->fpdf->SetFont('Arial','',10);
$this->fpdf->cell(20,5,'SEXO',1,0,'C',TRUE);
$this->fpdf->cell(20,5,'EDAD',1,0,'C',TRUE);
$this->fpdf->cell(20,5,'Estado Civil',1,0,'C',TRUE);
$this->fpdf->cell(22.16,5,'C. Propia',1,0,'C',TRUE);
$this->fpdf->cell(22.16,5,'C. Rentada',1,0,'C',TRUE);
$this->fpdf->cell(22.16,5,utf8_decode('Años Res'),1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Ing. Sem.',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Ing. Extra',1,0,'C',TRUE);
$this->fpdf->cell(21.1,5,'Total',1,0,'C',TRUE);
$this->fpdf->SetFont('Arial','',12);
$this->fpdf->Ln();
$c1 = $solicitud->casa_propia == 0?'NO':'SI';
$c2 = $solicitud->casa_rentada == 0?'NO':'SI';
$this->fpdf->cell(20,5,$solicitud->sexo,1,0,'C');
$this->fpdf->cell(20,5,$solicitud->edad,1,0,'C');
$this->fpdf->cell(20,5,$solicitud->estado_civil,1,0,'C');
$this->fpdf->cell(22.16,5,$c1,1,0,'C');
$this->fpdf->cell(22.16,5,$c2,1,0,'C');
$this->fpdf->cell(22.16,5,$solicitud->anos_residiendo,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->ingreso_semanal,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->ingreso_extra,1,0,'C');
$this->fpdf->cell(21.1,5,$solicitud->total,1,0,'C');
/* Fin AVAL */
/* Referencias personales*/
$this->fpdf->SetFont('Arial','B',12);
$this->fpdf->SetXY(10,160);
$this->fpdf->SetFillColor(191,191,191);
$this->fpdf->cell(30,6,'Referencias personales',0,2,'L');
$this->fpdf->SetFont('Arial','',10);
foreach($this->db->get_where('clientes_referencias',array('cliente'=>$solicitud->clienteid))->result() as $r)
{
    $this->fpdf->cell(20,5,'Parentesco',1,0,'C',TRUE);
    $this->fpdf->cell(40,5,'Nombre(s)',1,0,'C',TRUE);
    $this->fpdf->cell(44.5,5,'Apellido Paterno',1,0,'C',TRUE);
    $this->fpdf->cell(43.5,5,'Apellido Materno',1,0,'C',TRUE);
    $this->fpdf->cell(21.1,5,'Telefono',1,0,'C',TRUE);
    $this->fpdf->cell(21.1,5,'Celular',1,0,'C',TRUE);
    $this->fpdf->Ln();
    $this->fpdf->cell(20,5,$r->parentesco,1,0,'C');
    $this->fpdf->cell(40,5,$r->nombre,1,0,'C');
    $this->fpdf->cell(44.5,5,$r->apellido_paterno,1,0,'C');
    $this->fpdf->cell(43.5,5,$r->apellido_materno,1,0,'C');
    $this->fpdf->cell(21.1,5,$r->telefono,1,0,'C');
    $this->fpdf->cell(21.1,5,$r->celular,1,0,'C');
    $this->fpdf->Ln();
}
/* Fin referencias personales */

/* Bienes declarados */
$this->fpdf->SetFont('Arial','B',12);
$this->fpdf->Ln();

$this->fpdf->SetFillColor(191,191,191);
$this->fpdf->cell(30,6,'Bienes declarados',0,2,'L');
$this->fpdf->SetFont('Arial','',10);
foreach($this->db->get_where('clientes_bienes',array('cliente'=>$solicitud->clienteid))->result() as $r)
{
    $this->fpdf->cell(25,5,'Articulo',1,0,'C',TRUE);
    $this->fpdf->cell(20,5,'Marca',1,0,'C',TRUE);
    $this->fpdf->cell(59.5,5,'Caracteristicas',1,0,'C',TRUE);
    $this->fpdf->cell(43.5,5,'Numero de serie',1,0,'C',TRUE);
    $this->fpdf->cell(21.1,5,'T. de uso',1,0,'C',TRUE);
    $this->fpdf->cell(21.1,5,'Valor',1,0,'C',TRUE);
    $this->fpdf->Ln();
    $this->fpdf->cell(25,5,substr($r->articulo,0,8).'...',1,0,'C');
    $this->fpdf->cell(20,5,$r->marca,1,0,'C');
    $this->fpdf->cell(59.5,5,$r->caracteristicas,1,0,'C');
    $this->fpdf->cell(43.5,5,$r->numero_serie,1,0,'C');
    $this->fpdf->cell(21.1,5,utf8_decode($r->tiempo_uso),1,0,'C');
    $this->fpdf->cell(21.1,5,$r->valor_estimado,1,0,'C');
    $this->fpdf->Ln();
    
}

$this->fpdf->Line(30,260,90,260);
$this->fpdf->Line(110,260,170,260);
$this->fpdf->SetXY(30,260);
$this->fpdf->cell(60,5,'Nombre y firma del solicitante',0,0,'C');
$this->fpdf->cell(20,5,'',0,0,'C');
$this->fpdf->cell(60,5,'Nombre y firma del aval',0,0,'C');
/* Fin  Bienes declaradoss */
$this->fpdf->Output();
?>
