<?php
$pdf = $this->fpdf;
$pdf->AddPage('L');
$pdf->SetFillColor(142,180,227);
/*Cabecera*/
$pdf->SetFont('Arial','B',14);
$pdf->cell(0,14,'RESUMEN GLOBAL DE PROMOTORES',0,2,'C');
$pdf->SetFont('Arial','',10);
$pdf->Image(base_url('img/logo.png'), 10 ,10, 50 , 20,'PNG');
$pdf->Ln();
$pdf->cell(0,5,'Semana: '.date("d/m/Y",strtotime($fecha)),0,2,'L');
$pdf->Ln();
$pdf->cell(41,5,'Promotor',1,0,'L',true);
$pdf->cell(21,5,'Deb. Entregar',1,0,'C',true);
$pdf->cell(21,5,'Entrego',1,0,'C',true);
$pdf->cell(21,5,'Diferencia',1,0,'C',true);
$pdf->cell(21,5,'Recuperado',1,0,'C',true);
$pdf->cell(21,5,'Rec TV',1,0,'C',true);
$pdf->cell(21,5,'Deuda',1,0,'C',true);
$pdf->cell(21,5,'Credito',1,0,'C',true);
$pdf->cell(41,5,'Com. Apertura',1,0,'C',true);
$pdf->cell(21,5,'Comision',1,0,'C',true);
$pdf->cell(21,5,'Abono',1,0,'C',true);
$t1 = $t2 = $t3 = $t4 = $t5 = $t6 = $t7 = $t8 = $t9 = $t10 = 0;
foreach($this->db->get('promotores')->result() as $e)
{
    $pdf->Ln();
    $pdf->cell(41,5,utf8_decode($e->nombre),1,0,'L');
    
    $abono = 0;
    $pago = 0;
    $saldo = 0;
    $solicitud = 0;
    foreach($this->db->get_where('estado_cuenta',array('promotor'=>$e->id))->result() as $x)
    {
        $saldo += $solicitud!=$x->solicitud?$this->db->get_where('estado_cuenta',array('solicitud'=>$x->solicitud))->row()->saldo:0;
        $solicitud = $x->solicitud;
        $abono+=$x->abono;
        $pago+=$x->pago;
    }
    $pdf->cell(21,5,number_format($abono,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($pago,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($abono-$pago,2,',','.'),1,0,'C');
    $pdf->cell(21,5,0,1,0,'C');
    $pdf->cell(21,5,0,1,0,'C');
    $pdf->cell(21,5,number_format($abono-$pago,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($saldo,2,',','.'),1,0,'C');
    $pdf->cell(41,5,number_format($abono*0.05,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($abono*0.07,2,',','.'),1,0,'C');
    $pdf->cell(21,5,number_format($abono*0.125,2,',','.'),1,0,'C');
    $t1 += $abono;
    $t2 += $pago;
    $t3 += $abono-$pago;
    $t4 += 0;
    $t5 += 0;
    $t6 += $abono-$pago;
    $t7 += $saldo;
    $t8 += $abono*0.05;
    $t9 += $abono*0.07;
    $t10+= $abono*0.125;
}
$pdf->Ln();
$pdf->cell(41,5,'Totales',1,0,'L',true);
$pdf->cell(21,5,number_format($t1,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t2,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t3,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t4,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t5,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t6,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t7,2,',','.'),1,0,'C',true);
$pdf->cell(41,5,number_format($t8,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t9,2,',','.'),1,0,'C',true);
$pdf->cell(21,5,number_format($t10,2,',','.'),1,0,'C',true);
$pdf->Output();
?>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

