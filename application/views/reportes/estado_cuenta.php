<?php
$pdf = $this->fpdf;
$pdf->AddPage('L');
$pdf->SetFillColor(142,180,227);
/*Cabecera*/
$pdf->SetFont('Arial','B',14);
$pdf->cell(0,14,'ESTADO DE CUENTA PERSONAL',0,2,'C');
$pdf->SetFont('Arial','',10);
$pdf->Image(base_url('img/logo.png'), 10 ,10, 50 , 20,'PNG');
$pdf->Ln();
$pdf->cell(40,5,'DESCRIPCION',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->plazon),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'INTERES CON IVA',0,0,'L');
$pdf->cell(40,5,number_format($cta->row()->saldo-$cta->row()->monto_aprobado,2,',','.'),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'PROMOTOR',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->promotorn),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'ZONA',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->sucursaln),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'CLIENTE',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->clienten.' '.$cta->row()->clientea),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'DIRECCION',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->cliented),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'AVAL',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->avaln.' '.$cta->row()->avala),0,0,'L');
$pdf->Ln();
$pdf->cell(40,5,'DIRECCION',0,0,'L');
$pdf->cell(40,5,utf8_decode($cta->row()->avald),0,0,'L');
$pdf->Ln();
$ref = $this->db->get_where('clientes_referencias',array('cliente'=>$cta->row()->clienteid));
if($ref->num_rows>0){
$pdf->cell(40,5,'REFERENCIA 1',0,0,'L');
$pdf->cell(40,5,utf8_decode($ref->row()->nombre.' '.$ref->row()->apellido_paterno),0,0,'L');
$pdf->Ln();
}
if($ref->num_rows>1){
$pdf->cell(40,5,'REFERENCIA 2',0,0,'L');
$pdf->cell(40,5,utf8_decode($ref->row(1)->nombre.' '.$ref->row(1)->apellido_paterno),0,0,'L');
$pdf->Ln();
}
$pdf->Ln();
/* Fin  Cabecera */
/* Tabla de estado de cuenta */
$pdf->cell(14,5,'',0,0,'L');
$pdf->cell(25,5,'CREDITO',1,0,'C',true);
$pdf->cell(27,5,$cta->row()->monto_aprobado,1,0,'C');
$pdf->cell(50,5,'ABONO SEMANAL',1,0,'C',true);
$pdf->cell(25,5,$cta->row()->abono,1,0,'C');
$pdf->cell(23,5,'',0,0,'C');
$pdf->cell(50,5,'SALDO INICIAL',1,0,'C',true);
$pdf->cell(25,5,$cta->row()->saldo,1,0,'C');
$pdf->Ln();
$pdf->cell(14,5,'No',1,0,'L',true);
$pdf->cell(25,5,'Fecha',1,0,'C',true);
$pdf->cell(27,5,'DEBE ABONAR',1,0,'C',true);
$pdf->cell(25,5,'PAGO',1,0,'C',true);
$pdf->cell(25,5,'A CAPITAL',1,0,'C',true);
$pdf->cell(25,5,'INTERES',1,0,'C',true);
$pdf->cell(23,5,'IVA 16%',1,0,'C',true);
$pdf->cell(25,5,'FALTANTE',1,0,'C',true);
$pdf->cell(25,5,'INT MORA',1,0,'C',true);
$pdf->cell(25,5,'IVA MORA',1,0,'C',true);
$pdf->cell(25,5,'SALDO REAL',1,0,'C',true);
$pdf->Ln();
$x = 0;
$a = 0;
$p = 0;
$ca = 0;
$i = 0;
$in = 0;
$f = 0;
$m = 0;
$im = 0;
$s = 0;
foreach($cta->result() as $c)
{
    if($x>0){
    $no = $x>0?$x:'';
    $pdf->cell(14,5,$no,1,0,'L');
    $pdf->cell(25,5,date("d/m/Y",strtotime($c->fecha)),1,0,'C');
    $pdf->cell(27,5,number_format($c->abono,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->pago,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->capital,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->interes,2,',','.'),1,0,'C');
    $pdf->cell(23,5,number_format($c->iva,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->faltante,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->mora,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->iva_mora,2,',','.'),1,0,'C');
    $pdf->cell(25,5,number_format($c->saldo,2,',','.'),1,0,'C');
    $a+= $c->abono;
    $p+= $c->pago;
    $ca+= $c->capital;
    $in+= $c->interes;
    $i+= $c->iva;
    $f+= $c->faltante;
    $m+= $c->mora;
    $im+= $c->iva_mora;    
    $pdf->Ln(); 
    }
    $x++;
}
    $pdf->cell(14,5,'',1,0,'L',true);
    $pdf->cell(25,5,'Totales',1,0,'C',true);
    $pdf->cell(27,5,number_format($a,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($p,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($ca,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($in,2,',','.'),1,0,'C',true);
    $pdf->cell(23,5,number_format($i,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($f,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($m,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($im,2,',','.'),1,0,'C',true);
    $pdf->cell(25,5,number_format($c->saldo,2,',','.'),1,0,'C',true);
/* Fin de tabla */
$pdf->setXY(120,35);
$pdf->cell(27,5,'No ZONA.',1,0,'L');
$pdf->cell(27,5,$cta->row()->sucursalid,1,0,'R');
$pdf->Ln();
$pdf->setXY(120,40);
$pdf->cell(27,5,'No CLIENTE.',1,0,'L');
$pdf->cell(27,5,$cta->row()->clienteid,1,0,'R');
$pdf->Ln();
$pdf->setXY(120,45);
$pdf->cell(27,5,'CREDITO No.',1,0,'L');
$pdf->cell(27,5,$cta->row()->solicitudid,1,0,'R');
$pdf->Ln();

$pdf->setXY(190,35);
$pdf->cell(27,5,'MONTO',1,0,'L');
$pdf->cell(27,5,number_format($cta->row()->monto_aprobado,2,',','.'),1,0,'R');
$pdf->Ln();
$pdf->setXY(190,40);
$pdf->cell(27,5,'PAGO TOTAL',1,0,'L');
$pdf->cell(27,5,number_format($a,2,',','.'),1,0,'R');
$pdf->Ln();
$pdf->setXY(190,45);
$pdf->cell(27,5,'INICIO',1,0,'L');
$pdf->cell(27,5,date("d/m/Y",strtotime($cta->row()->fecha)),1,0,'R');
$pdf->Ln();
$pdf->setXY(190,50);
$pdf->cell(27,5,'TERMINO',1,0,'L');
$pdf->cell(27,5,date("d/m/Y",strtotime($cta->row($cta->num_rows-1)->fecha)),1,0,'R');
$pdf->Ln();
$this->fpdf->Output();
?>
