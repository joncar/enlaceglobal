<!Doctype html>
<html lang="es">
	<head>
		<title><?= empty($title)?'Enlaceglobal.mx':$title ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="description" content="<?= !empty($description)?$description:'' ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/font-awesome.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/fonts.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; ?>                
                <?php else: ?>
                <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
                <?php endif; ?>                
                <!-- Respond.js proxy on external server -->                 
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>               
	</head>
	<body>   
            <header>
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?= site_url() ?>"><i class="fa fa-home"></i> Inicio</a>
                      </div>

                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <?php if(!empty($_SESSION['user'])): ?>
                            <ul class="nav navbar-nav">               
                                <?php foreach($this->db->get('controladores')->result() as $c): ?>
                                 <?= $this->querys->get_menu($c->nombre) ?>              
                                 <?php endforeach ?>
                             </ul>
                          <?php endif ?>
                          <?php if(!empty($_SESSION['user'])): ?>
                          <ul class="nav navbar-nav navbar-right">                                             
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $_SESSION['nombre'].' '.$_SESSION['apellido'] ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="<?= base_url('panel/datos') ?>">Editar perfil</a></li>
                                  <li><a href="<?= base_url('main/unlog') ?>">Salir</a></li>                                  
                                </ul>
                              </li>
                          </ul>
                          <?php endif ?>
                      </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                  </nav>
            </header>            
            <?php $this->load->view($view) ?>                                    
        </body>
</html>