<div class="row" style="margin:0px;">
    <div class="col-xs-3 col-sm-2 well" id="leftbar">
        <?= img('img/logo.png','width:100%') ?>            
        <div class="panel-group" id="accordion">            
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#one">
                    <?= ucfirst($this->router->fetch_class()) ?>
                  </a>
                </h4>
              </div>
              <div id="one" class="panel-collapse collapse in">
                <div class="panel-body">
                  <ul class="nav nav-stacked" style="text-align:left">  
                    <?= $this->querys->get_menu($this->router->fetch_class(),FALSE) ?>
                  </ul>
                </div>
              </div>
            </div> 
        </div>
    </div>
    <div class="col-xs-9" id="rightbar">        
        <?php if(!empty($crud)): $this->load->view('cruds/'.$crud);?>
        <?php else: ?>
        
        <?php endif ?>
    </div>
</div>