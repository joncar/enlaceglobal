<div class="row">    
    <div class="col-xs-12">
        <p><b>Semanas pendientes por abono</b></p>
        <?= $output ?>
        <p><b>Resumen de sucursal</b></p>
        <div class="panel-group" id="accordion2">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Resumen general
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                  <ul>
                    <li><b>Cantidad de clientes: </b><?php echo $this->db->get_where('clientes',array('user'=>$sucursal->user))->num_rows ?></li>
                    <li><b>Cantidad de prestamos: </b><?php echo $this->db->get_where('solicitudes',array('sucursal'=>$sucursal->id))->num_rows ?></li>                
                    <li><b>Total prestamos: </b><?php echo $this->db->query("select SUM(monto_aprobado) as saldo from solicitudes where sucursal='".$sucursal->id."'")->row()->saldo?></li>
                    <li><b>Total abonados: </b><?php echo $this->db->query("select SUM(pago) as saldo from solicitudes join estado_cuenta on estado_cuenta.solicitud = solicitudes.id where solicitudes.sucursal='".$sucursal->id."' AND estado_cuenta.actualizado='1'")->row()->saldo; ?></li>
                    <li><b>Total deudas: </b><?php echo $this->db->query("select SUM(abono) as saldo from solicitudes join estado_cuenta on estado_cuenta.solicitud = solicitudes.id where solicitudes.sucursal='".$sucursal->id."' AND estado_cuenta.actualizado='0'")->row()->saldo; ?></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                    Consultar estado de cuenta global
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                  <form action="<?= base_url('promotor/resumen_sucursal') ?>" method="get" onsubmit="return validar(this)">
                    Desde: <input type="text" name="desde" data-val="required" value="<?= date("d/m/Y") ?>">  Hasta: <input type="text" data-val="required" name="hasta" value="<?= date("d/m/Y") ?>">
                    <input type="hidden" name="sucursal" value="<?= $sucursal->id ?>">
                    <button type="submit" class="btn btn-info">Consultar</button>
                  </form>
                </div>
              </div>
            </div> 
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                    Buros
                  </a>
                </h4>
              </div>
              <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <a href="<?= base_url('promotor/buros/'.$sucursal->id) ?>">Consultar buros de esta sucursal</a>
                </div>
              </div>
            </div> 
          </div>
    </div>
</div>
<script>
    function send2(form,url)
    {
        send(form,url,function(){<?= refresh_list(false); ?>});
        return false;
    }
</script>