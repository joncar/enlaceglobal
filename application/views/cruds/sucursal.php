<div class="row">    
    <div class="col-xs-12">        
        <?= $output ?>
    </div>
</div>
<script>
    $(document).ready(function(){       
        $("#nro_field_box").before(añadir_field('Datos del cliente:','info'));        
        $("#nombre_field_box").before(añadir_field('Aval:','info'));        
    });
    
    function añadir_field(text,id)
    {
        return '<div id="'+id+'" class="form-field-box odd" style="font-size:20px; margin: 0 0 20px;">'+
                    
                    '<div id="nombre_input_box" class="form-input-box">'+
                    '<b><i class="fa fa-check-circle" style="color: rgb(160, 160, 255);"></i> '+text+'</b>'+   
                    '<div class="clear"></div>'+
                '</div>';
    }
</script>