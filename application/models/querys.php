<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    function has_access($controller,$function = '')
    {
        if(!is_numeric($controller))
            $con = $this->db->get_where('controladores',array('nombre'=>$controller));
        
        if(!empty($function) && !is_numeric($function) && (!empty($con) && $con->num_rows>0 || is_numeric($controller)))
            $fun = $this->db->get_where('funciones',array('nombre'=>$function,'controlador'=>$con->row()->id));
        
        $controller = !empty($con) && $con->num_rows>0?$con->row()->id:$controller;
        $function = !empty($fun) && $fun->num_rows>0?$fun->row()->id:$function;        
        
        if($_SESSION['cuenta']==0)
        {            
            if(!empty($function))
            return $this->db->get_where('roles',array('controlador'=>$controller,'funcion'=>$function,'user'=>$_SESSION['user']))->num_rows>0?TRUE:FALSE;
            else
            return $this->db->get_where('roles',array('controlador'=>$controller,'user'=>$_SESSION['user']))->num_rows>0?TRUE:FALSE;
        }
        else
            return TRUE;
    }
    
    function get_menu($controlador,$dropdown=TRUE)
    {
        
        $str = '';
        $this->db->select('controladores.id as controlador,funciones.etiqueta as label, funciones.id as funcion_id, controladores.nombre, funciones.nombre as funcion');
        $this->db->join('funciones','funciones.controlador = controladores.id');
        $menu = $this->db->get_where('controladores',array('controladores.nombre'=>$controlador,'funciones.visible'=>1));
        if($menu->num_rows>0 && $this->has_access($menu->row()->controlador)){
        $str .= $dropdown?'<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.  ucfirst($controlador).'<b class="caret"></b></a>
                <ul class="dropdown-menu">':'';
        foreach($menu->result() as $m){
        if($this->has_access($m->controlador,$m->funcion_id)){
        $label = empty($m->label)?ucwords($m->funcion):$m->label;
        $str.= '<li><a href="'.base_url($controlador.'/'.$m->funcion).'">'.$label.'</a></li>';
        }
        }
        $str.= $dropdown?'
                </ul>
              </li>':'';
        }
        return $str;
    }
    
    function get_solicitud($id)
    {
        $this->db->select(
                'solicitudes.*, 
                 sucursales.nombre as sucursalnombre, 
                 clientes.nombre as cliente_nombre, 
                    clientes.apellido_paterno as cliente_apellido_paterno, 
                    clientes.apellido_materno as cliente_apellido_materno,
                    clientes.id as clienteid,
                    clientes.direccion as cliente_direccion, 
                    clientes.ciudad as cliente_ciudad,
                    clientes.estado as cliente_estado,
                    clientes.colonia as cliente_colonia,
                    clientes.zip as cliente_zip,
                    clientes.folio_ife as cliente_ife,
                    clientes.negocio_actividad as cnegocio_actividad,
                    clientes.celular as ccelular,
                    clientes.telefono as ctelefono,
                    clientes.sexo as csexo,
                    clientes.edad as cedad,
                    clientes.estado_civil as cestado_civil,
                    clientes.casa_propia as ccasa_propia,
                    clientes.casa_rentada as ccasa_rentada,
                    clientes.anos_residiendo as canos,
                    clientes.ingreso_semanal as cingreso,
                    clientes.ingreso_extra as cextra,
                    clientes.total as ctotal,
                 plazos.nombre as plazo');                
        $this->db->join('sucursales','sucursales.id = solicitudes.sucursal');
        $this->db->join('plazos','plazos.id = solicitudes.plazo');
        $this->db->join('clientes','clientes.id = solicitudes.cliente');
        return $this->db->get_where('solicitudes',array('solicitudes.id'=>$id));        
    }
    function get_cuenta($solicitud)
    {
        $this->db->select(
                'estado_cuenta.*,plazos.nombre as plazon, solicitudes.monto_aprobado, promotores.nombre as promotorn,
                sucursales.nombre as sucursaln, sucursales.id as sucursalid,
                clientes.nombre as clienten, clientes.apellido_paterno as clientea, clientes.direccion as cliented, clientes.id as clienteid,
                solicitudes.nombre as avaln, solicitudes.apellido_paterno as avala, solicitudes.direccion as avald, solicitudes.id as solicitudid');                        
        $this->db->join('solicitudes','solicitudes.id = estado_cuenta.solicitud');
        $this->db->join('plazos','plazos.id = solicitudes.plazo');
        $this->db->join('promotores','promotores.id = solicitudes.promotor');
        $this->db->join('sucursales','sucursales.id = solicitudes.sucursal');
        $this->db->join('clientes','clientes.id = solicitudes.cliente');
        return $this->db->get_where('estado_cuenta',array('estado_cuenta.solicitud'=>$solicitud));        
    }
    
    function get_resumen($promotor)
    {
        $this->db->select('solicitudes.id, sucursales.id as sucid, sucursales.comision, sucursales.nombre as suc_nombre,promotores.nombre as pro_nombre');
        $this->db->join('sucursales','sucursales.id = solicitudes.sucursal');
        $this->db->join('promotores','promotores.id = solicitudes.promotor');
        return $this->db->get_where('solicitudes',array('promotor'=>$promotor));
    }
    
    function get_sucursal_global($sucursal,$desde,$hasta)
    {        
        if($sucursal->num_rows>0){
            $solicitudes = $this->db->get_where('solicitudes',array('sucursal'=>$sucursal->row()->id)); 
            foreach($solicitudes->result() as $s)
                $this->db->or_where("solicitud = '".$s->id."' AND fecha >= '".$desde."' AND fecha <= '".$hasta."'");
            return $this->db->get('estado_cuenta');
        }
    }
}
?>
