<?php
class Estado_cuenta  extends grocery_CRUD_Model  {

	function get_list()
        {
    	if($this->table_name === null)
    		return false;

    	$select = "`{$this->table_name}`.*";

    	//set_relation special queries
    	if(!empty($this->relation))
    	{
    		foreach($this->relation as $relation)
    		{
    			list($field_name , $related_table , $related_field_title) = $relation;
    			$unique_join_name = $this->_unique_join_name($field_name);
    			$unique_field_name = $this->_unique_field_name($field_name);

				if(strstr($related_field_title,'{'))
				{
					$related_field_title = str_replace(" ","&nbsp;",$related_field_title);
    				$select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
				}
    			else
    			{
    				$select .= ", $unique_join_name.$related_field_title AS $unique_field_name";
    			}

    			if($this->field_exists($related_field_title))
    				$select .= ", `{$this->table_name}`.$related_field_title AS '{$this->table_name}.$related_field_title'";
    		}
    	}

    	//set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
    	if(!empty($this->relation_n_n))
    	{
			$select = $this->relation_n_n_queries($select);
    	}

    	$this->db->select($select, false);

    	$results = $this->db->get($this->table_name);
        $abono = 0;
        $pago = 0;
        $capital = 0;
        $interes = 0;
        $iva = 0;
        $faltante = 0;
        $mora = 0;
        $iva_mora = 0;
        $saldo = 0;
        foreach($results->result() as $r)
        {
            $abono+= $r->abono;
            $pago+= $r->pago;
            $capital+= $r->capital;
            $interes += $r->interes;
            $iva += $r->iva ;
            $faltante += $r->faltante;
            $mora += $r->mora;
            $iva_mora += $r->iva_mora;
            $saldo = $r->saldo;
        }
        $abono = '<b>'.$abono.'</b>';
        $pago = '<b>'.$pago.'</b>';
        $capital = '<b>'.$capital.'</b>';
        $interes = '<b>'.$interes.'</b>';
        $iva = '<b>'.$iva.'</b>';
        $faltante = '<b>'.$faltante.'</b>';
        $mora = '<b>'.$mora.'</b>';
        $iva_mora = '<b>'.$iva_mora.'</b>';
        $saldo = '<b>'.$saldo.'</b>';
        $results->result_object[count($results->result_object)] = (object)array('interes'=>$interes,'iva'=>$iva,'faltante'=>$faltante,'mora'=>$mora,'iva_mora'=>$iva_mora,'saldo'=>$saldo,'id'=>'99','capital'=>$capital,'solicitud'=>'0','abono'=>$abono,'pago'=>$pago);
    	return $results->result();
        }
}
?>
