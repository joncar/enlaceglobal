<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		parent::index();
	}                            
        /*Cruds*/
        
        function controladores($x = '',$y = '')
        {
            $crud = parent::controladores($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        function funciones($x = '',$y = '')
        {
            $crud = parent::funciones($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function roles($x = '',$y = '')
        {
            $crud = parent::roles($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function usuarios($x = '',$y = '')
        {
            $crud = parent::usuarios($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function colonias($x = '',$y = '')
        {
            $crud = parent::colonias($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function estados($x = '',$y = '')
        {
            $crud = parent::estados($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function ciudades($x = '',$y = '')
        {
            $crud = parent::ciudades($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function sucursales($x = '',$y = '')
        {
            $crud = parent::sucursales($x,$y);
            $crud->set_relation('user','user','nombre');
            $crud->required_fields('nombre','user','comision');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function promotores($x = '',$y = '')
        {            
            $crud = parent::promotores($x,$y);                                                                        
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function plazos($x = '',$y = '')
        {
            $crud = parent::plazos($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function solicitudes($x = '',$y = '')
        {
            $crud = parent::solicitudes($x,$y);
            $crud->set_relation('promotor','promotores','nombre');
            $crud->set_relation('sucursal','sucursales','nombre');
            $crud->set_relation('user','user','nombre');
            $crud->columns('promotor','sucursal','fecha','cliente','monto_solicitado','monto_aprobado','status');
            $crud->unset_read();
            $crud->callback_after_update(array($this,'admin_solicitudes_aupdate'));
            $crud->callback_after_insert(array($this,'admin_solicitudes_aupdate'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function estado_cuenta($x = '',$y = '')
        {
            $crud = parent::estado_cuenta($x,$y);
            $crud->where('estado_cuenta.solicitud',$x);                       
            $crud->set_model('estado_cuenta');
            $crud->unset_add()                 
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud->edit_fields('pago','faltante','mora','iva_mora','saldo','actualizado');
            $crud->field_type('faltante','invisible')
                 ->field_type('mora','invisible')
                 ->field_type('iva_mora','invisible')
                 ->field_type('saldo','invisible')
                 ->field_type('actualizado','hidden',1);
            $crud->required_fields('pago');
            $crud->callback_edit_field('pago',function($val,$pr){            
            $val = $val==0?get_instance()->db->get_where('estado_cuenta',array('id'=>$pr))->row()->abono:$val;
            return form_input('pago',$val,'id="field-pago"');                
            });
            $crud->callback_before_update(array($this,'promotor_estado_cuenta_bupdate'));
            $output = $crud->render();
            if(is_numeric($x))$output->x = $x;
            $output->view = 'panel';
            $output->crud = 'estado_cuenta';
            $this->loadView($output); 
        }
        
        function resumen_semanal()
        {
            if(!empty($_GET['fecha'])){
            $promotor = $this->db->get_where('promotores',array('user'=>$_SESSION['user']));
            parent::imprimir_resumen_semanal_promotores($_GET['fecha']);
            }
            else
            $this->loadView(array('view'=>'cruds/user','output'=>'Seleccione una fecha <br/><form action="'.base_url('admin/resumen_semanal').'" onsubmit="return validar(this)" method="get"><input type="text" name="fecha" value="'.date("d/m/Y").'" data-val="required"></form>'));
        }
        
        function clientes($x='',$y='')
        {
            $crud = parent::clientes($x,$y);              
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         function control($x = '',$y = '',$z = '')
        {
            $suc = $x;
            if(empty($x)){
                $crud = parent::sucursales($x,$y);  
                $crud->display_as('nombre','Seleccione una zona');
                $crud->columns('nombre');
                $crud->callback_column('nombre',function($val,$row){return '<a href="'.base_url('promotor/'.$this->router->fetch_method().'/'.$row->id).'">'.$val.'</a>';});                
                $crud->unset_add()                 
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();                        
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $this->loadView($output);
            }
            else
            {
                $crud = parent::estado_cuenta();
                $solicitudes = $this->db->get_where('solicitudes',array('sucursal'=>$x,'status'=>1));
                if($solicitudes->num_rows>0){
                $w = '';
                $x = 0;
                foreach($solicitudes->result() as $f)
                {
                    $s = $x==0?'':' OR ';
                    $w.= $s."estado_cuenta.actualizado=0 AND estado_cuenta.solicitud=".$f->id." AND estado_cuenta.fecha<='".date("Y-m-d")."'";
                    $x++;
                }
                $crud->where($w,null,FALSE);
                }
                else $crud->where('id',0);
                
                $crud->unset_add()                 
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();                                    
                $crud->columns('solicitud','cliente','fecha','abono','pago','capital','interes','iva','faltante','mora','saldo');
                $crud->callback_column('cliente',function($val,$row){
                    get_instance()->db->select('clientes.*');
                    get_instance()->db->join('clientes','clientes.id = solicitudes.cliente');
                    $cliente = get_instance()->db->get_where('solicitudes',array('solicitudes.id'=>$row->solicitud));
                    return $cliente->row()->nombre.' '.$cliente->row()->apellido_paterno;
                });
                $crud->callback_column('pago',function($val,$row){
                    return '<form onsubmit="return send2(this,\''.base_url('promotor/estado_cuenta/'.$row->solicitud.'/update/'.$row->id).'\')">
                            <input type="hidden" name="actualizado" value="1"> 
                            '.form_input('pago',$row->abono,'id="field-pago"').
                            '</form>';
                });
                $crud->display_as('solicitud','Numero de solicitud');
                $output = $crud->render();
                $output->sucursal = $this->db->get_where('sucursales',array('id'=>$suc))->row();
                $output->view = 'panel';
                $output->crud = 'control'; 
                $this->loadView($output);
            }
        }
        
        function admin_solicitudes_aupdate($post,$id)
        {
            if($post['status']==1 && $this->db->get_where('estado_cuenta',array('solicitud'=>$id))->num_rows==0)
            {
                $this->db->select('solicitudes.*,plazos.*');
                $this->db->join('plazos','plazos.id = solicitudes.plazo');
                $d = $this->db->get_where('solicitudes',array('solicitudes.id'=>$id))->row();
                $d->fecha = traer_lunes($d->fecha);
                //Calcular abono semanal
                $data = array(); 
                $data['sucursal'] = $d->sucursal;
                $data['promotor'] = $d->promotor;
                $data['cliente'] = $d->cliente;
                $data['solicitud'] = $id;
                $data['abono'] = $post['monto_aprobado']*$d->factor_abono;
                $data['capital'] = $data['abono']*$d->factor_capital;
                $data['interes'] = $data['abono']*$d->factor_interes;
                $data['iva'] = $data['abono']*$d->factor_iva;
                $data['saldo'] = $data['abono']*$d->semanas;
                for($i=0;$i<=$d->semanas;$i++){
                    $data['actualizado'] = $i==0?1:0;
                    $data['fecha'] = $i==0?$d->fecha:date("Y-m-d",strtotime('+'.($i*7).' days '.$d->fecha));                
                    $this->db->insert('estado_cuenta',$data);
                }
            }
        }
        
        function generar_tablas()
        {
            $solicitudes = $this->db->get('solicitudes');
            foreach($solicitudes->result() as $s)
            {
                $post = array('monto_aprobado'=>$s->monto_aprobado,'status'=>$s->status);
                $this->admin_solicitudes_aupdate($post,$s->id);
            }
        }
        
        function referencias($x = '',$y = '')
        {            
            $crud = parent::referencias($x,$y);            
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');       
            $crud->unset_fields('monto_aprobado','promotor');                        
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function bienes($x = '',$y = '')
        {            
            $crud = parent::bienes($x,$y); 
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');       
            $crud->unset_fields('monto_aprobado','promotor');                        
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function buros($x = '',$y = '')
        {
            $suc = $x;
            if(empty($x)){
               $crud = parent::sucursales($x,$y);  
                $crud->display_as('nombre','Seleccione una sucursal');
                $crud->columns('nombre');
                $crud->callback_column('nombre',function($val,$row){return '<a href="'.base_url('admin/'.$this->router->fetch_method().'/'.$row->id).'">'.$val.'</a>';});                
                $crud->unset_add()                 
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();                        
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $this->loadView($output);
            }
            else
            {
                $crud = parent::buros();
                $crud->where('sucursal',$x);
                $crud->unset_add()                 
                     ->unset_delete()
                     ->unset_read()                     
                     ->unset_export()
                     ->unset_edit();                 
                $output = $crud->render();                
                $output->view = 'panel';
                $output->crud = 'user';
                $this->loadView($output);
            }
        }
        /*Callbacks*/        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */