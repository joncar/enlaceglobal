<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Zonas extends Panel {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		parent::index();
	}                            
        /*Cruds*/
        public function clientes($x = '',$y = '')
        {
            $crud = parent::clientes($x,$y);
            $crud->set_model('user_privilege');
            $crud->unset_fields('user');
            $crud->where('user',$_SESSION['user']);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de clientes';
            $this->loadView($output);
        }
        
        function buros()
        {
            $sucursal = $this->db->get_where('sucursales',array('user'=>$_SESSION['user']))->row()->id;
            $crud = parent::buros();
            $crud->where('sucursal',$sucursal);
            $crud->unset_add()                 
                 ->unset_delete()
                 ->unset_read()                     
                 ->unset_export()
                 ->unset_edit();                 
            $output = $crud->render();                
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function resumen_semanal()
        {
            $sucursal = $this->db->get_where('sucursales',array('user'=>$_SESSION['user']));
            if($sucursal->num_rows>0)
            parent::imprimir_estado_cuenta_global($sucursal->row()->id,traer_lunes(date("Y-m-d")),traer_lunes(date("Y-m-d")));
            else
            header("Location:".base_url('zonas/sucursales/add'));
        }
        
        function sucursales($x = '',$y = '')
        {
            $crud = parent::sucursales($x,$y); 
            $crud->set_relation('user','user','nombre');
            $crud->set_model('user_privilege');
            $crud->where('sucursales.user',$_SESSION['user']);
            $crud->unset_fields('user');
            if($this->db->get_where('sucursales',array('user'=>$_SESSION['user']))->num_rows>0)
                $crud->unset_add();
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->unset_fields('comision','user');                                    
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function solicitudes($x = '',$y = '')
        {
            $sucursal = $this->db->get_where('sucursales',array('user'=>$_SESSION['user']));
            if($sucursal->num_rows==0)
                header("Location:".base_url('zonas/sucursales'));
            $crud = parent::solicitudes($x,$y);
            if($x!='read')
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}',array('user'=>$_SESSION['user']));       
            else
            $crud->set_relation('cliente','clientes','id',array('user'=>$_SESSION['user']));       
            $crud->set_model('user_privilege');
            if($x!='read')$crud->unset_fields('user','monto_aprobado','promotor','status');
            $crud->where('solicitudes.user',$_SESSION['user']);                        
            $crud->field_type('sucursal','hidden',$sucursal->row()->id);
            $crud->unset_delete()
                 ->unset_print()
                 ->unset_edit()
                 ->unset_export(); 
            $crud->set_relation('promotor','promotores','nombre');
            $crud->columns('id','sucursal','cliente','monto_solicitado','monto_aprobado','status');
            $output = $crud->render();
            if($x=='read' && is_numeric($y))
            $_SESSION['solicitud'] = $y;
            $output->view = 'panel';
            $output->crud = 'solicitud';
            $this->loadView($output); 
        }
        
        function estado_cuenta($x = '',$y = '')
        {
            $crud = parent::estado_cuenta($x,$y);            
            $crud->where('estado_cuenta.solicitud',$x);                       
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->unset_columns('solicitud');              
            $output = $crud->render();
            if(is_numeric($x))$output->x = $x;
            $output->view = 'panel';
            $output->crud = 'estado_cuenta';
            $this->loadView($output); 
        }
        
        function referencias($x = '',$y = '')
        {            
            $crud = parent::referencias($x,$y);            
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}',array('user'=>$_SESSION['user']));       
            $crud->unset_fields('monto_aprobado','promotor');                        
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function bienes($x = '',$y = '')
        {            
            $crud = parent::bienes($x,$y); 
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}',array('user'=>$_SESSION['user']));       
            $crud->unset_fields('monto_aprobado','promotor');                        
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        
        function get_prestamos()
        {
            $this->form_validation->set_rules('cliente','Cliente','required|integer|greather_than[0]');
            if($this->form_validation->run())
            {
                $prestamos = 0;
                foreach($this->db->get_where('solicitudes',array('cliente'=>$this->input->post('cliente'),'status'=>1))->result() as $s)
                {
                    $this->db->order_by('fecha','DESC');
                    if($this->db->get_where('estado_cuenta',array('solicitud'=>$s->id))->row()->saldo>0)
                     $prestamos++;
                }
                if($prestamos>0)echo 'Este cliente posee '.$prestamos.' Prestamos vigente';
            }
        }
        /*Callbacks*/        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */