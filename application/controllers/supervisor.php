<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Supervisor extends Panel {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		parent::index();
	}                            
        /*Cruds*/                        
        function solicitudes($x = '',$y = '')
        {
            $crud = parent::solicitudes($x,$y);
            $crud->unset_add()
                 ->unset_delete();
            $crud->unset_fields('user','monto_aprobado','promotor','status','sucursal','cliente');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        /*Callbacks*/        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */