<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Promotor extends Panel {        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		parent::index();
	}                            
        /*Cruds*/
        function promotores($x = '',$y = '')
        {                        
            $crud = parent::promotores($x,$y);                
            if($this->db->get_where('promotores',array('user'=>$_SESSION['user']))->num_rows>0)
                $crud->unset_add();
            $crud->set_model('user_privilege');
            $crud->unset_fields('user');
            $crud->where('promotores.user',$_SESSION['user']);
            $crud->unset_delete()
                 ->unset_read()                 
                 ->unset_print()
                 ->unset_export();            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output); 
        }
        function solicitudes($x = '',$y = '')
        {            
            $promotor = $this->db->get_where('promotores',array('user'=>$_SESSION['user']));
            if($promotor->num_rows==0)
                header("Location:".base_url('promotor/promotores'));
            $crud = parent::solicitudes($x,$y);
            if($x!='read')
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');       
            else
            $crud->set_relation('cliente','clientes','id');    
            $crud->field_type('promotor','hidden',$promotor->row()->id);            
            $crud->field_type('status','dropdown',array('0'=>'Por aprobar','1'=>'Aprobado'));  
            if($x!='read'){
            $crud->callback_edit_field('monto_aprobado',function($val,$pr){            
            $val = $val==0?get_instance()->db->get_where('solicitudes',array('id'=>$pr))->row()->monto_solicitado:$val;
            return form_input('monto_aprobado',$val,'id="field-monto_aprobado"');                
            });
            }
            $crud->columns('promotor','sucursal','fecha','cliente','monto_solicitado','monto_aprobado','status');
            $crud->set_relation('sucursal','sucursales','nombre');
            $crud->set_relation('promotor','promotores','nombre');
            $crud->unset_delete()                 
                 ->unset_add()                 
                 ->unset_print()
                 ->unset_export();
            $crud->display_as('nro','Numero de solicitud');
            if($x!='read'){$crud->edit_fields('monto_aprobado','promotor','status','fecha');}
            else $_SESSION['solicitud'] = $y;
            $crud->callback_after_update(array($this,'promotor_solicitudes_aupdate'));
            $crud->callback_column('nro',function($val,$row){return '<a href="'.base_url('promotor/solicitudes/read/'.$row->id).'">'.$row->sucursal.'s'.$row->id.'</a>';});
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'sucursal';
            $this->loadView($output); 
        }
        
        function estado_cuenta($x = '',$y = '')
        {
            $crud = parent::estado_cuenta($x,$y);
            $crud->where('estado_cuenta.solicitud',$x);                       
            $crud->set_model('estado_cuenta');
            $crud->unset_add()                 
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud->edit_fields('pago','faltante','mora','iva_mora','saldo','actualizado');
            $crud->field_type('faltante','invisible')
                 ->field_type('mora','invisible')
                 ->field_type('iva_mora','invisible')
                 ->field_type('saldo','invisible')
                 ->field_type('actualizado','hidden',1);
            $crud->required_fields('pago');
            $crud->callback_edit_field('pago',function($val,$pr){            
            $val = $val==0?get_instance()->db->get_where('estado_cuenta',array('id'=>$pr))->row()->abono:$val;
            return form_input('pago',$val,'id="field-pago"');                
            });
            $crud->callback_before_update(array($this,'promotor_estado_cuenta_bupdate'));
            $output = $crud->render();
            if(is_numeric($x))$output->x = $x;
            $output->view = 'panel';
            $output->crud = 'estado_cuenta';
            $this->loadView($output); 
        }
        
        function control($x = '',$y = '',$z = '')
        {
            $suc = $x;
            if(empty($x)){
                $this->loadView(parent::suc_sel($x, $y));
            }
            else
            {
                $crud = parent::estado_cuenta();
                $solicitudes = $this->db->get_where('solicitudes',array('sucursal'=>$x,'status'=>1));
                if($solicitudes->num_rows>0){
                $w = '';
                $x = 0;
                foreach($solicitudes->result() as $f)
                {
                    $s = $x==0?'':' OR ';
                    $w.= $s."estado_cuenta.actualizado=0 AND estado_cuenta.solicitud=".$f->id." AND estado_cuenta.fecha<='".date("Y-m-d")."'";
                    $x++;
                }
                $crud->where($w,null,FALSE);
                }
                else $crud->where('id',0);
                
                $crud->unset_add()                 
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();                                    
                $crud->columns('solicitud','cliente','fecha','abono','pago','capital','interes','iva','faltante','mora','saldo');
                $crud->callback_column('cliente',function($val,$row){
                    get_instance()->db->select('clientes.*');
                    get_instance()->db->join('clientes','clientes.id = solicitudes.cliente');
                    $cliente = get_instance()->db->get_where('solicitudes',array('solicitudes.id'=>$row->solicitud));
                    return $cliente->row()->nombre.' '.$cliente->row()->apellido_paterno;
                });
                $crud->callback_column('pago',function($val,$row){
                    return '<form onsubmit="return send2(this,\''.base_url('promotor/estado_cuenta/'.$row->solicitud.'/update/'.$row->id).'\')">
                            <input type="hidden" name="actualizado" value="1"> 
                            '.form_input('pago',$row->abono,'id="field-pago"').
                            '</form>';
                });
                $crud->display_as('solicitud','Numero de solicitud');
                $output = $crud->render();
                $output->sucursal = $this->db->get_where('sucursales',array('id'=>$suc))->row();
                $output->view = 'panel';
                $output->crud = 'control'; 
                $this->loadView($output);
            }
        }
        
        
        
        function buros($x = '',$y = '')
        {
            $suc = $x;
            if(empty($x)){
               $this->loadView(parent::suc_sel($x, $y));
            }
            else
            {
                $crud = parent::buros();
                $crud->where('sucursal',$x);
                $crud->unset_add()                 
                     ->unset_delete()
                     ->unset_read()                     
                     ->unset_export()
                     ->unset_edit();                 
                $output = $crud->render();                
                $output->view = 'panel';
                $output->crud = 'user';
                $this->loadView($output);
            }
        }
        /*Callbacks*/        
        function promotor_solicitudes_aupdate($post = '',$id = 1)
        {                              
            if($post['status']==1 && $this->db->get_where('estado_cuenta',array('solicitud'=>$id))->num_rows==0)
            {
                $this->db->select('solicitudes.*,plazos.*');
                $this->db->join('plazos','plazos.id = solicitudes.plazo');
                $d = $this->db->get_where('solicitudes',array('solicitudes.id'=>$id))->row();
                $d->fecha = traer_lunes($d->fecha);
                //Calcular abono semanal
                $data = array(); 
                $data['sucursal'] = $d->sucursal;
                $data['promotor'] = $d->promotor;
                $data['cliente'] = $d->cliente;
                $data['solicitud'] = $id;
                $data['abono'] = $post['monto_aprobado']*$d->factor_abono;
                $data['capital'] = $data['abono']*$d->factor_capital;
                $data['interes'] = $data['abono']*$d->factor_interes;
                $data['iva'] = $data['abono']*$d->factor_iva;
                $data['saldo'] = $data['abono']*$d->semanas;
                for($i=0;$i<=$d->semanas;$i++){
                    $data['actualizado'] = $i==0?1:0;
                    $data['fecha'] = $i==0?$d->fecha:date("Y-m-d",strtotime('+'.($i*7).' days '.$d->fecha));                
                    $this->db->insert('estado_cuenta',$data);
                }
            }
        }
        
        function promotor_estado_cuenta_bupdate($post,$id)
        {
            $d = $this->db->get_where('estado_cuenta',array('id'=>$id));
            $d = $d->row();
            //calcular toda la tabla denuevo
            if($d->actualizado==1){                                
                foreach($this->db->get_where('estado_cuenta',array('solicitud'=>$d->solicitud))->result() as $n=>$f)
                {       
                    $e = $this->db->get_where('estado_cuenta',array('id'=>$f->id))->row();                    
                        if($e->id==$d->id){                    
                            $d->saldo = $e->saldo;
                            $post = $this->modificar_estado($d,$post);                           
                        }
                        else{
                            $saldo = ($e->saldo-$e->pago)+$e->faltante+$e->mora+$e->iva_mora;
                            $this->db->where('solicitud',$e->solicitud);
                            $this->db->where('id >= ',$e->id);
                            $this->db->update('estado_cuenta',array('saldo'=>$saldo));
                        }
                }                
            }
            if($d->actualizado==0){
            $n = $d->num_rows;
            $post = $this->modificar_estado($d,$post);
            }
            return $post;
        }
        
        function resumen_semanal()
        {
            if(!empty($_GET['fecha'])){
            $promotor = $this->db->get_where('promotores',array('user'=>$_SESSION['user']));
            parent::imprimir_resumen_semanal($promotor,$_GET['fecha']);
            }
            else
            $this->loadView(array('view'=>'cruds/user','output'=>'Seleccione una fecha <br/><form action="'.base_url('promotor/resumen_semanal').'" onsubmit="return validar(this)" method="get"><input type="text" name="fecha" value="'.date("d/m/Y").'" data-val="required"></form>'));
        } 
        
        function resumen_sucursal()
        {
            if(!empty($_GET['desde']) && !empty($_GET['hasta']) && !empty($_GET['sucursal'])){
                $_GET['desde'] = str_replace("/","-",$_GET['desde']);
                $_GET['hasta'] = str_replace("/","-",$_GET['hasta']);
                parent::imprimir_estado_cuenta_global($_GET['sucursal'],$_GET['desde'],$_GET['hasta']);
            }
            else
            $this->loadView(array('view'=>'404'));
        }
        
        function modificar_estado($d,$post)
        {
            $post['faltante'] = $d->abono-$post['pago'];
            if($post['faltante']>0)
                $this->db->insert('buros',array('solicitud'=>$d->solicitud,'cliente'=>$d->cliente,'fecha'=>$d->fecha,'sucursal'=>$d->sucursal,'abono'=>$d->abono,'pago'=>$post['pago'],'deuda'=>$post['faltante']));
            $post['faltante'] = $post['faltante']>=0?$post['faltante']:0;
            $post['mora'] = $post['faltante'];
            $post['iva_mora'] = $post['mora']*0.16;
            $post['saldo'] = ($d->saldo-$post['pago'])+$post['faltante']+$post['mora']+$post['iva_mora'];                        
            $post['saldo'] = $post['saldo']<0?0:$post['saldo'];
            $this->db->where('solicitud',$d->solicitud);
            $this->db->where('id > ',$d->id);
            $this->db->update('estado_cuenta',array('saldo'=>$post['saldo']));
            
            //Si se excede la semana pautada para le pago se penaliza con 100 mas por semana, y se incluye el registro
            $this->db->order_by('id','DESC');            
            if($post['saldo']>0 && $d->id==$this->db->get_where('estado_cuenta',array('solicitud'=>$d->solicitud))->row()->id)
            $this->db->insert('estado_cuenta',array('solicitud'=>$d->solicitud,'fecha'=>date("Y-m-d",strtotime("+7 days ".$d->fecha)),'abono'=>$d->abono,'capital'=>$d->capital,'interes'=>$d->interes,'iva'=>$d->iva));            
            $this->db->update('estado_cuenta',array('saldo'=>($post['saldo']+100)),array('id'=>$this->db->insert_id()));
            return $post;
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */