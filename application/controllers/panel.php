<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
       
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('panel');
	}
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url());
            else
            parent::loadView($crud);
        }                
        /*Cruds*/          
        public function controladores($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('controladores');
            $crud->set_subject('Controlador');      
            //Fields
            //unsets            
            //Displays           
            //Fields types            
            //Validations              
            $crud->required_fields('nombre');
            //Callbacks            
            return $crud;          
	}
        
        public function funciones($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('funciones');
            $crud->set_subject('Funcion');      
            $crud->set_relation('controlador','controladores','nombre');                        
            //Fields
            //unsets            
            //Displays           
            $crud->display_as('Visible','Visible en el menu');
            //Fields types            
            //Validations              
            //Callbacks            
            $crud->callback_column('visible',function($val){return $val==1?'Si':'No';});
            $crud->callback_field('visible',function($val){return form_dropdown('visible',array('0'=>'No','1'=>'Si'),$val,'id="field-visible"');});
            $crud->required_fields('nombre','visible','controlador');
            return $crud;          
	}
        
        public function roles($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('roles');
            $crud->set_subject('Rol');  
            $crud->set_relation('user','user','{nombre} {apellido_paterno}');
            $crud->set_relation('controlador','controladores','nombre');
            $crud->set_relation('funcion','funciones','nombre');
            $crud->set_relation_dependency('funcion','controlador','controlador');
            $crud->display_as('user','Usuario')
                 ->display_as('update','Accesos');
            $crud->field_type('insert','invisible');
            $crud->field_type('select','invisible');
            $crud->field_type('delete','invisible');
            $crud->callback_field('update',array($this,'update_rolesField'));                        
            $crud->field_type('created','invisible');          
            $crud->field_type('modified','invisible');  
            $crud->required_fields('user','controlador');
            $crud->callback_before_insert(array($this,'roles_binsertion'));
            $crud->callback_after_insert(array($this,'roles_ainsertion'));
            return $crud;            
	}
        
        
        public function colonias($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('colonias');
            $crud->set_subject('Colonias');                                                   
            //Fields            
            //unsets                        
            //Displays            
            //Fields types            
            //Validations  
            $crud->required_fields('nombre');              
            //Callbacks            
            return $crud;          
	}
        
        public function estados($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('estados');
            $crud->set_subject('Estado');                           
            $crud->set_relation('colonia','colonias','nombre');            
            
            //Fields
            //unsets                        
            //Displays            
            //Fields types            
            //Validations  
            $crud->required_fields('nombre','colonia');              
            //Callbacks            
            return $crud;          
	}
        
        public function ciudades($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ciudades');
            $crud->set_subject('Ciudades');                           
            $crud->set_relation('colonia','colonias','nombre');
            $crud->set_relation('estado','estados','nombre');
            $crud->set_relation_dependency('estado','colonias','colonia');            
            //Fields            
            //unsets                        
            //Displays            
            //Fields types            
            //Validations  
            $crud->required_fields('nombre','colonia','estado');              
            return $crud;          
	}
        
        public function usuarios($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuario');                           
            $crud->set_relation('colonia','colonias','nombre');
            $crud->set_relation('estado','estados','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            $crud->set_relation_dependency('estado','colonias','colonia');            
            $crud->set_relation_dependency('ciudad','estados','estado');            
            //Fields   
            $crud->field_type('password','password');         
            $crud->columns('nombre','apellido_paterno','apellido_materno','usuario','email');
            //unsets                        
            //Displays            
            $crud->display_as('admin','Superadministrador');
            //Fields types  
            $crud->field_type('sexo','enum',array('M','F'));          
            $crud->field_type('estado_civil','enum',array('Soltero(a)','Casado(a)','Viudo(a)','Divorciado(a)'));          
            $crud->field_type('casa_propia','dropdown',array('0'=>'No','1'=>'Si'));          
            $crud->field_type('casa_rentada','dropdown',array('0'=>'No','1'=>'Si')); 
            $crud->field_type('status','dropdown',array('1'=>'Activo','0'=>'Bloqueado'));          
            $crud->field_type('admin','dropdown',array('0'=>'No','1'=>'Si'));          
            //Validations  
            $crud->required_fields('nombre','password','colonia','estado','ciudad','apellido_paterno','email','usuario','direccion','zip','folio_ife'
                   ,'negocio_actividad','celular','telefono','sexo','edad','estado_civil','casa_propia','casa_rentada','anos_residiendo','status','admin');              
            $crud->set_rules('password','Password','required|greather_than[8]');
            $crud->callback_before_insert(array($this,'user_binsertion'));
            $crud->callback_before_update(array($this,'user_bupdate'));
            return $crud;          
	}
        
        public function sucursales($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('sucursales');
            $crud->set_subject('Sucursales');    
            //$crud->set_relation('user','user','nombre');
            //Fields               
            //unsets               
            //Displays                        
            //Fields types              
            //Validations  
            $crud->required_fields('nombre');
            return $crud;          
	}
        
         public function promotores($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('promotores');
            $crud->set_subject('Promotores');    
            $crud->set_relation('user','user','nombre');
            //Fields               
            //unsets               
            //Displays                        
            //Fields types              
            //Validations  
            $crud->required_fields('nombre','user');
            return $crud;          
	}
        
        public function plazos($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('plazos');
            $crud->set_subject('Plazos');                                       
            //Fields                           
            //unsets                        
            //Displays                             
            //Fields types                          
            //Validations  
            $crud->required_fields('nombre','factor_abono','factor_iva','factor_capital','factor_interes');
            $crud->set_rules('factor_abono','Factor Abono','greather_than[0]');
            $crud->set_rules('factor_iva','Factor IVA','greather_than[0]');
            $crud->set_rules('factor_capital','Factor Capital','greather_than[0]');
            $crud->set_rules('factor_interes','Factor Interes','greather_than[0]');
            return $crud;          
	}
        
        public function clientes($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('clientes');
            $crud->set_subject('Clientes');                           
            $crud->set_relation('colonia','colonias','nombre');
            $crud->set_relation('estado','estados','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            $crud->set_relation_dependency('estado','colonias','colonia');            
            $crud->set_relation_dependency('ciudad','estados','estado');                                    
            //Fields               
            $crud->columns('nombre','apellido_paterno','apellido_materno');
            //unsets                        
            //Displays        
            $crud->display_as('nro','Numero del cliente');                
            
            //Fields types              
            $crud->field_type('sexo','enum',array('M','F'));          
            $crud->field_type('estado_civil','enum',array('Soltero(a)','Casado(a)','Viudo(a)','Divorciado(a)'));          
            $crud->field_type('casa_propia','dropdown',array('0'=>'No','1'=>'Si'));          
            $crud->field_type('casa_rentada','dropdown',array('0'=>'No','1'=>'Si')); 
            //Validations  
            $crud->required_fields('nombre','edad','colonia','estado','ciudad','apellido_paterno','usuario','direccion','zip'
                   ,'negocio_actividad','celular','telefono','sexo','estado_civil','casa_propia','casa_rentada','anos_residiendo'
                   ,'status','admin','ingreso_semanal','ingreso_extra','total');                                      
            return $crud;          
	}
        
        public function solicitudes($x = '',$y = '')
	{
            
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('clientes');
            $crud->set_table('solicitudes');
            $crud->set_subject('Solicitudes');                           
            $crud->set_relation('colonia','colonias','nombre');
            $crud->set_relation('estado','estados','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');
            $crud->set_relation('plazo','plazos','nombre');            
            $crud->set_relation_dependency('estado','colonias','colonia');            
            $crud->set_relation_dependency('ciudad','estados','estado');                                    
            if(is_numeric($x))
                $crud->where('solicitudes.sucursal',$x);
            //Fields               
            $crud->columns('cliente','fecha','monto_solicitado','monto_aprobado','status');
            //unsets                        
            //Displays       
            $crud->display_as('fecha','Fecha de solicitud');      
            $crud->add_action('Estado de cuenta','',base_url($this->router->fetch_class().'/estado_cuenta').'/');
            //Fields types  
            $crud->field_type('status','dropdown',array('0'=>'Por aprobar','1'=>'Aprobado','2'=>'Rechazado'));
            $crud->field_type('sexo','enum',array('M','F'));          
            $crud->field_type('estado_civil','enum',array('Soltero(a)','Casado(a)','Viudo(a)','Divorciado(a)'));          
            $crud->field_type('casa_propia','dropdown',array('0'=>'No','1'=>'Si'));          
            $crud->field_type('casa_rentada','dropdown',array('0'=>'No','1'=>'Si')); 
            $crud->add_action('<i class="fa fa-print"></i>','',base_url($this->router->fetch_class().'/imprimir_solicitud').'/');
            //Validations  
            $crud->required_fields('nombre','monto_solicitado','monto_aprobado','promotor','fecha','edad','colonia','estado','ciudad','apellido_paterno','usuario','direccion','zip'
                   ,'negocio_actividad','plazo','celular','telefono','sexo','estado_civil','casa_propia','casa_rentada','anos_residiendo'
                   ,'status','admin','ingreso_semanal','ingreso_extra','total','cliente','sucursal','');                                      
            $crud->callback_column('status',function($val){return $val==0?'Por aprobar':'Aprobado';});
            $crud->callback_column('s'.substr(md5('sucursal'),0,8),function($val,$row){return '<a href="'.base_url(get_instance()->router->fetch_class().'/solicitudes/'.$row->sucursal).'">'.$val.'</a>';});
            return $crud;          
	}
        
        public function referencias($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('clientes_referencias');
            $crud->set_subject('Referencia');                                       
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');            
            //Fields                           
            //unsets                        
            //Displays                                            
            //Fields types              
            //Validations  
            $crud->required_fields('nombre','parentesco','apellido_paterno','apellido_materno','telefono','celular');                                      
            return $crud;          
	}
        
        public function bienes($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('clientes_bienes');
            $crud->set_subject('Bien');                                       
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');            
            //Fields                           
            //unsets                        
            //Displays                                            
            //Fields types              
            //Validations  
            $crud->required_fields('marca','articulo','caracteristica','numero_serie','tiempo_uso','valor_estimado');                                      
            return $crud;          
	}
        
         public function buros($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('buros');
            $crud->set_subject('Buros');                                       
            $crud->set_relation('cliente','clientes','{nombre} {apellido_paterno}');
            $crud->set_relation('sucursal','sucursales','nombre');     
            $crud->field_type('status','dropdown',array('0'=>'Activo','1'=>'Vencido'));
            //Fields                           
            //unsets                        
            //Displays                                            
            //Fields types              
            //Validations  
            
            return $crud;          
	}
        
        function estado_cuenta($x = '',$y = '')
        {
            $crud = new ajax_grocery_CRUD();            
            $crud->set_theme('flexigrid');
            $crud->set_table('estado_cuenta');
            $crud->set_subject('Estado_cuenta');            
            //Fields
            //unsets
            //Displays
            $crud->display_as('abono','Debe abonar')
                 ->display_as('pago','Pagó')
                 ->display_as('capital','A capital')
                 ->display_as('mora','INT Mora')
                 ->display_as('iva_mora','IVA MORA')
                 ->display_as('saldo','Saldo Real');
            
            $crud->columns('nro','fecha','abono','pago','capital','interes','iva','faltante','mora','iva_mora','saldo');
            $crud->callback_column('nro',function($val,$row){
                $n = 0;
                $x = 0;
                get_instance()->db->order_by('fecha','ASC');
                foreach(get_instance()->db->get_where('estado_cuenta',array('solicitud'=>$row->solicitud))->result() as $e)
                {
                    if($n==0)
                    {
                        $x++;
                        if($row->fecha==$e->fecha)$n = 1;
                    }
                }
                if($row->solicitud==0)return '<b>Totales</b>';
                return $x>0?$x-1:0;
            });
            //Fields types
            //Validations            
            $crud->required_fields('fecha','abono','capital','interes','iva','saldo','solicitud','pago');
            return $crud;          
        }
        
        function suc_sel($x,$y)
        {
            $crud = $this->sucursales($x,$y);  
            $crud->display_as('nombre','Seleccione una zona');
            $crud->columns('nombre');
            $crud->callback_column('nombre',function($val,$row){return '<a href="'.base_url('promotor/'.$this->router->fetch_method().'/'.$row->id).'">'.$val.'</a>';});                
            $crud->unset_add()                 
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->unset_edit();                        
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            return $output;
        }
                
        /*Callbacks*/     
        function roles_binsertion($post)
        {
            if(empty($post['funcion'])){
                $post['funcion'] = $this->db->get_where('funciones',array('controlador'=>$post['controlador']))->row()->id;
                $this->all = TRUE;
            }
            else
                $this->all = FALSE;
            return $post;
        }
        
        function roles_ainsertion($post)
        {            
            if($this->all)
            {
                $this->db->delete('roles',array('user'=>$post['user']));
                $x = 0;
                foreach($this->db->get_where('funciones',array('controlador'=>$post['controlador']))->result() as $y)
                {                    
                    if($this->db->get_where('roles',array('controlador'=>$post['controlador'],'funcion'=>$y->id,'user'=>$post['user']))->num_rows==0)
                    $this->db->insert('roles',array('controlador'=>$post['controlador'],'funcion'=>$y->id,'user'=>$post['user']));                    
                }                
            }
            return true;
        }
        
        function user_binsertion($post)
        {
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function user_bupdate($post,$id)
        {
            if($this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password'])
                    $post['password'] = md5($post['password']);
            return $post;
        }
        
        function imprimir_solicitud($id)
        {
            $_POST['id'] = $id;
            $this->form_validation->set_rules('id','ID','required|integer|greather_than[0]');
            if($this->form_validation->run())
            {                
                $this->load->view('reportes/solicitud',array('solicitud'=>$this->querys->get_solicitud($id)->row()));
            }
            else
                $this->loadView('404');
        }
        
        function imprimir_estado_cuenta($id)
        {
            $_POST['id'] = $id;
            $this->form_validation->set_rules('id','ID','required|integer|greather_than[0]');
            if($this->form_validation->run())
            {
                $this->load->view('reportes/estado_cuenta',array('cta'=>$this->querys->get_cuenta($id)));
            }
            else
                $this->loadView('404');
        }
        
        function imprimir_resumen_semanal($promotor,$fecha)
        {
            if($promotor->num_rows>0){                
                $fecha = str_replace("/","-",$fecha);
                $this->db->group_by('sucursales.nombre');
                $this->load->view('reportes/resumen_semanal',array('promotor'=>$promotor,'fecha'=>$fecha,'res'=>$this->querys->get_resumen($promotor->row()->id)));
            }
            else $this->loadView('404');
        }
        
        function imprimir_resumen_semanal_promotores($fecha)
        {            
            $fecha = str_replace("/","-",$_GET['fecha']);            
            $this->load->view('reportes/resumen_semanal_promotores',array('fecha'=>$fecha));
        }
        
        function imprimir_estado_cuenta_global($sucursal,$desde,$hasta)
        {
            $desde = traer_lunes($desde);
            $hasta = traer_lunes($hasta);
            $sucursal = $this->db->get_where('sucursales',array('id'=>$sucursal));
            $this->db->group_by('fecha');
            $cta = $this->querys->get_sucursal_global($sucursal,$desde,$hasta);
            if($cta->num_rows>0)
            $this->load->view('reportes/resumen_sucursal',array('sucursal'=>$sucursal,'cta'=>$cta,'desde'=>$desde,'hasta'=>$hasta));            
            else
            echo '<h1>No se encontraron actividades para esta sucursal</h1>';
        }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
