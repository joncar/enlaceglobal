<?php 
	function  correo($email = 'joncar.c@gmail.com',$titulo = '',$msj = '',$from='info@softwopen.com')
        {            
            $sfrom=$from; //cuenta que envia
            $sdestinatario=$email; //cuenta destino
            $ssubject=$titulo; //subject
            $shtml=$msj; //mensaje
            $sheader="From:".$sfrom."\nReply-To:".$sfrom."\n";
            $sheader=$sheader."X-Mailer:PHP/".phpversion()."\n";
            $sheader=$sheader."Mime-Version: 1.0\n";
            $sheader=$sheader."Content-Type: text/html; charset:utf-8";
            mail($sdestinatario,$ssubject,$shtml,$sheader);
        }
        function meses($mes)
        {
            $meses = array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
            return $meses[$mes-1];
        }

        function contratos_tipo($val)
        {
            switch($val)
            {
                case 0: return 'ARRENDAMIENTO';
                case 1: return 'VENTA A PLAZO';
                case 2: return 'VENTA DE CONTADO';
                case 3: return 'VENTA A CRÉDITO';
            }
        }
        
        function rol($var)
        {
            switch($var)
            {
                case 0: return 'Remitente';
                case 1: return 'Destinatario';
                case 2: return 'Recepcionista';
                case 3: return 'Administrador';
            }
        }
        
        function status($var)
        {
            switch($var)
            {
                case 0: return 'Bloqueado';
                case 1: return 'Activo';
            }
        }
        
        function traer_lunes($fecha)
        {
            $dia = date("w",strtotime($fecha));
            $dia = $dia>0?$dia:7;
            $dias = $dia-1;
            $dias = $dias<0?$dias*-1:$dias;
            return date("Y-m-d",strtotime('-'.$dias.' days '.$fecha));
        }
        
        function refresh_list($label = true)
        {
            $l = $label?'<script>':'';
            $r = $label?'</script>':'';
            return $l."$('.filtering_form').trigger('submit')".$r;
        }
?>